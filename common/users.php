<?php

/**
 * Creates new user account.
 *
 * @param  string $username
 * @param  string $password (in plain text)
 * @return bool   TRUE for success, FALSE otherwise
 */
function registerUser($username, $password)
{
	global $dbLink;
	$passwordHash = password_hash($password, PASSWORD_BCRYPT);
	return mysqli_query($dbLink, '
		INSERT INTO `users`
		(`name`, `password_hash`) VALUES (
			"' . mysqli_real_escape_string($dbLink, $username) . '",
			"' . mysqli_real_escape_string($dbLink, $passwordHash) . '"
		)
	');
}


/**
 * Attemps to log-in the user.
 *
 * @param  string $username
 * @param  string $password (in plain text)
 * @return bool   TRUE for success, FALSE otherwise
 */
function loginUser($username, $password)
{
	global $dbLink;
	$result = mysqli_query($dbLink, '
		SELECT *
		FROM `users`
		WHERE `name` = "' . mysqli_real_escape_string($dbLink, $username) . '"'
	);
	$row = mysqli_fetch_assoc($result);

	if ($row === NULL) {
		return FALSE; // incorrect username
	}

	if (!password_verify($password, $row['password_hash'])) {
		return FALSE; // incorrect password
	}

	session_regenerate_id(TRUE); // prevents session fixation
	unset($row['password_hash']);
	$_SESSION['user'] = $row;
	return TRUE;
}


/**
 * Logouts the user.
 *
 * @return void
 */
function logoutUser()
{
	unset($_SESSION['user']);
}


/**
 * Redirects user who is not logged in to index.php.
 *
 * @return void
 */
function requireLoggedInUser()
{
	if (!isset($_SESSION['user'])) {
		redirect('index', [
			'fid' => addFlashMessage('Stránka vyžaduje přihlášení.', 'error'),
		]);
	}
}
