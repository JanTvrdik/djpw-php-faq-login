<?php

// === Configure PHP ===========================================================

error_reporting(E_ALL);
ini_set('display_errors', 'On');


// === HTTP headers ============================================================

header('Content-Type: text/html; charset=utf-8');
session_start();


// === Global variables ========================================================

/** @var mysqli $dbLink */
$dbLink = mysqli_connect('localhost', 'root', '', 'muj_projekt');
mysqli_set_charset($dbLink, 'utf8');
mysqli_report(MYSQLI_REPORT_ERROR);

/** @var string $basePath */
$basePath = substr($_SERVER['SCRIPT_NAME'], 0, strrpos($_SERVER['SCRIPT_NAME'], '/'));


// === Core functions ==========================================================

/**
 * Renders a template with given parameters.
 *
 * @param  string $page   template name (e.g. 'index')
 * @param  array  $params
 * @return void
 */
function renderTemplate($page, array $params = [])
{
	extract($params, EXTR_SKIP);
	$page = __DIR__ . "/../templates/$page.php";
	require __DIR__ . "/../templates/layout.php";
}


/**
 * Redirects to a given page with given parameters.
 *
 * @param  string $page   page name (e.g. 'index')
 * @param  array  $params
 * @return void
 */
function redirect($page, array $params = [])
{
	global $basePath;
	$params = http_build_query($params);
	header("Location: $basePath/$page.php" . ($params ? "?$params" : ""));
	exit;
}


/**
 * Adds flash message to session and return its ID.
 *
 * @param  string $message
 * @param  string $type    e.g. 'info' or 'error'
 * @return string
 */
function addFlashMessage($message, $type = 'info')
{
	$fid = dechex(mt_rand(0, 1000000));
	$_SESSION['flashMessages'][$fid] = [
		'message' => $message,
		'type' => $type,
	];
	return $fid;
}


/**
 * Returns flash message from session identified by ID.
 *
 * @param  string $id
 * @return array|NULL
 */
function getFlashMessage($fid)
{
	if (isset($_SESSION['flashMessages'][$fid])) {
		$message = $_SESSION['flashMessages'][$fid];
		unset($_SESSION['flashMessages'][$fid]);
		return $message;
	}
}
