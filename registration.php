<?php

require __DIR__ . '/common/init.php';
require __DIR__ . '/common/users.php';

$error = NULL;
if (isset($_POST['username'], $_POST['password']) && is_string($_POST['username']) && is_string($_POST['password'])) {
	if (registerUser($_POST['username'], $_POST['password'])) {
		redirect('index', [
			'fid' => addFlashMessage('Registrace úspěšně proběhla.'),
		]);

	} else {
		$error = 'Při registraci došlo k chybě.';
	}
}

renderTemplate('registration', [
	'title' => 'Registrace',
	'error' => $error,
]);
