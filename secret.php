<?php

require __DIR__ . '/common/init.php';
require __DIR__ . '/common/users.php';

requireLoggedInUser();
renderTemplate('secret', [
	'title' => 'Tajná stránka',
]);
