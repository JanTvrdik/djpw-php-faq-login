<!doctype html>
<title><?=htmlspecialchars($title) ?> | Můj web</title>
<link rel="stylesheet" href="style.css">

<h1><?=htmlspecialchars($title) ?></h1>

<?php if (isset($_GET['fid']) && is_string($_GET['fid']) && $flash = getFlashMessage($_GET['fid'])): ?>
	<div class="flash <?=htmlspecialchars($flash['type']) ?>"><?=htmlspecialchars($flash['message']) ?></div>
<?php endif; ?>

<?php require $page; ?>
