<?php if (isset($_SESSION['user'])): ?>
	<p>Přihlášen: <?=htmlspecialchars($_SESSION['user']['name']) ?> (id #<?=$_SESSION['user']['id'] ?>)</p>
	<p>Můžeš se <a href="secret.php">podívat na tajnou stránku</a> nebo se <a href="logout.php">odhlásit</a>.</p>
<?php else: ?>
	<form method="post">
		<?php if (isset($error)): ?>
			<p class="error"><?=htmlspecialchars($error) ?></p>
		<?php endif; ?>
		<label>Jméno: <input name="username" type="text"></label><br>
		<label>Heslo: <input name="password" type="password"></label><br>
		<input name="submit" type="submit" value="Přihlásit">
	</form>
<?php endif; ?>

<p><a href="registration.php">Zaregistrovat se</a></p>
