<?php

require __DIR__ . '/common/init.php';
require __DIR__ . '/common/users.php';

$error = NULL;
if (isset($_POST['username'], $_POST['password']) && is_string($_POST['username']) && is_string($_POST['password'])) {
	if (loginUser($_POST['username'], $_POST['password'])) {
		redirect('index', [
			'fid' => addFlashMessage('Byl jsi přihlášen.'),
		]);

	} else {
		$error = 'Zadal jsi neplatné uživatelské jméno nebo heslo.';
	}
}

renderTemplate('index', [
	'title' => 'Přihlášení',
	'error' => $error,
]);
