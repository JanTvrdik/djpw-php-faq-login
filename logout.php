<?php

require __DIR__ . '/common/init.php';
require __DIR__ . '/common/users.php';

logoutUser();
redirect('index', [
	'fid' => addFlashMessage('Byl jsi úspěšně odhlášen.'),
]);
